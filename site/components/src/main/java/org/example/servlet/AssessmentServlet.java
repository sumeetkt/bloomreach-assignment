package org.example.servlet;

import org.hippoecm.hst.site.HstServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;

public class AssessmentServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(AssessmentServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) {
        Repository repository =
                HstServices.getComponentManager().getComponent(Repository.class.getName());
        Session session = null;
        try {
            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
            Node rootNode = session.getRootNode();
            Node documentsNode = rootNode.getNode("content/documents");
            printDocumentNodeRecursive(documentsNode);

            printUserEnteredContentDetails(session);
        } catch (RepositoryException repositoryException) {
            log.info("Exception thrown {}", repositoryException.getMessage());
            throw new WebApplicationException(repositoryException);
        }
    }

    private void printDocumentNodeRecursive(Node node) {
        try {
            log.info("Name of the node : {}", node.getName());
            if (!node.isNodeType("hippofacnav:facetnavigation")) {
                NodeIterator nodeIterator = node.getNodes();
                nodeIterator.forEachRemaining((childNode) -> printDocumentNodeRecursive((Node) childNode));
            }
        } catch (RepositoryException repositoryException) {
            log.info("Exception thrown while iterating node {}", repositoryException.getMessage());
            throw new WebApplicationException(repositoryException);
        }
    }

    private void printUserEnteredContentDetails(Session session) throws RepositoryException {
        QueryManager queryManager = session.getWorkspace().getQueryManager();
        Query query = queryManager.createQuery("//*[jcr:contains(.,'fox')]", "xpath");
        QueryResult queryResult = query.execute();
        if (queryResult.getNodes().getSize() == 0) {
            log.info("Search string fox not found");
        }
        NodeIterator nodeIterator = queryResult.getNodes();
        nodeIterator.forEachRemaining((node) -> {
            try {
                printUserEnteredTextFromProperties((Node) node);
            } catch (RepositoryException repositoryException) {
                log.info("Exception thrown while iterating node {}", repositoryException.getMessage());
                throw new WebApplicationException(repositoryException);
            }
        });
    }

    private void printUserEnteredTextFromProperties(Node node) throws RepositoryException {
            PropertyIterator propertyIterator = node.getProperties();
            propertyIterator.forEachRemaining((property) -> {
                try {
                    printProperty((Property) property);
                } catch (RepositoryException repositoryException) {
                    log.info("Exception thrown while iterating property {}", repositoryException.getMessage());
                    throw new WebApplicationException(repositoryException);
                }
            });
    }

    private void printProperty(Property property) throws RepositoryException {
        log.info("Properties containing fox {}", property.getName());
    }

}
