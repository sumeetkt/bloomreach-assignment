package org.example.resource;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.jaxrs.services.AbstractResource;
import org.hippoecm.hst.util.PathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Produces({MediaType.APPLICATION_JSON})
@Path("/assessment-api/nodes")
public class AssessmentResource extends AbstractResource {

    private static final Logger log = LoggerFactory.getLogger(AssessmentResource.class);

    @GET
    @Path("/")
    public List<String> getNodeNames(
            @Context HttpServletRequest servletRequest,
            @Context HttpServletResponse servletResponse,
            @Context UriInfo uriInfo) {

        List<String> nodes = new ArrayList<>();
        try {
            HstRequestContext requestContext = RequestContextProvider.get();
            String mountContentPath =
                    requestContext.getResolvedMount().getMount()
                            .getContentPath();
            Node mountContentNode =
                    requestContext.getSession().getRootNode()
                            .getNode(PathUtils.normalizePath(mountContentPath));
            getNodeDetails(mountContentNode, nodes);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return nodes;
    }

    @GET
    @Path("/{uuid}/")
    public List<String> getNodeNamesByUUID(
            @Context HttpServletRequest servletRequest,
            @Context HttpServletResponse servletResponse,
            @Context UriInfo uriInfo,
            @NotNull @PathParam("uuid") String uuid) {

        List<String> nodes = new ArrayList<>();
        try {
            HstRequestContext requestContext = RequestContextProvider.get();
            QueryManager queryManager = requestContext.getSession().getWorkspace().getQueryManager();
            getNodeByUUID(queryManager, uuid, nodes);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return nodes;
    }

    private void getNodeByUUID(QueryManager queryManager, String uuid, List<String> nodes) {
        try {
            Query query = queryManager.createQuery("//*[@hippo:paths='" + uuid + "']", "xpath");
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            iterateOverNodes(nodes, nodeIterator);
        } catch (RepositoryException repositoryException) {
            log.info("Exception thrown while iterating node {}", repositoryException.getMessage());
            throw new WebApplicationException(repositoryException);
        }
    }

    private void iterateOverNodes(List<String> nodes, NodeIterator nodeIterator) {
        nodeIterator.forEachRemaining((childNode) -> {
            try {
                getNodeDetails((Node) childNode, nodes);
            } catch (RepositoryException repositoryException) {
                log.info("Exception thrown while iterating node {}", repositoryException.getMessage());
                throw new WebApplicationException(repositoryException);
            }
        });
    }

    private void getNodeDetails(Node node, List<String> nodes) throws RepositoryException {
        log.info("Name of the node : {}", node.getName());
        nodes.add(node.getName());
        if (!node.isNodeType("hippofacnav:facetnavigation")) {
            NodeIterator nodeIterator = node.getNodes();
            nodeIterator.forEachRemaining((childNode) -> {
                try {
                    getNodeDetails((Node) childNode, nodes);
                } catch (RepositoryException repositoryException) {
                    log.info("Exception thrown while iterating node {}", repositoryException.getMessage());
                    throw new WebApplicationException(repositoryException);
                }
            });
        }
    }
}
