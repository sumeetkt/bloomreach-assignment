package org.example.bean;

import org.hippoecm.hst.content.beans.ContentNodeBinder;
import org.hippoecm.hst.content.beans.ContentNodeBindingException;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

import javax.jcr.Node;

public class NodeBean extends HippoDocument implements ContentNodeBinder {
    @Override
    public boolean bind(Object o, Node node) throws ContentNodeBindingException {
        return false;
    }
}
